<?php

namespace Drupal\shoelace_styleguide;

trait ShoelaceStyleTrait {

  /**
   * Create custom options with non-numeric key values.
   *
   * @param array $values
   *
   * @return array
   */
  public function createOptions(array $values): array {
    $options = [];
    foreach ($values as $value) {
      $options[$value] = $value;
    }
    return $options;
  }

  /**
   * @param string $component
   * @return mixed
   */
  abstract public function loadComponent(string $component): mixed;

}
