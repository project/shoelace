<?php

namespace Drupal\shoelace_styleguide\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\sdc\ComponentPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShoelaceStyleguideController extends ControllerBase {

  private ComponentPluginManager $singleDirectoryComponent;

  /**
   * {@inheritdoc}
   */
  public function __construct(ComponentPluginManager $componentPluginManager) {
    $this->singleDirectoryComponent = $componentPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.sdc')
    );
  }

  /**
   * @return array
   */
  public function index(): array {
    $shoelaceComponents = $this->singleDirectoryComponent->getAllComponents();
    $components = [];
    foreach($shoelaceComponents as $shoelaceComponent) {
      $definition = $shoelaceComponent->getPluginDefinition();
      $componentInformation = $definition['information'];
      $components[$definition['name']] = [
        '#theme' => 'shoelace_card',
        '#header' => [
          'default' => $definition['name'],
          'icon' => [
            'name' => $componentInformation['icon'],
            'label' => 'Card Component'
          ],
        ],
        '#footer' => [
          'button' => [
            'preview' => [
              'default' => 'Preview',
              'variant' => 'primary',
              'size' => 'medium',
              'target' => '_self',
              'href' => Url::fromRoute(
                'shoelace_styleguide.preview',
                ['component' => 'shoelace:' . strtolower($definition['name'])],
                ['absolute' => TRUE]
              )
            ],
            'documentation' => [
              'default' => 'Documentation',
              'variant' => 'default',
              'target' => '_blank',
              'size' => 'medium',
              'href' => $componentInformation['docLink']
            ],
          ],
        ],
        '#default' => $definition['description'],
        '#attached' => [
          'library' => [
            'shoelace_styleguide/shoelace_styleguide',
          ],
        ]
      ];
    }

    // Sort components by alphabetical order.
    ksort($components);

    return $components;
  }

}
