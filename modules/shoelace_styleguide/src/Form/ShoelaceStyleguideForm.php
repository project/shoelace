<?php

namespace Drupal\shoelace_styleguide\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ScrollTopCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\sdc\ComponentPluginManager;
use Drupal\sdc\Exception\ComponentNotFoundException;
use Drupal\shoelace_styleguide\ShoelaceStyleTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShoelaceStyleguideForm extends FormBase {

  use ShoelaceStyleTrait;

  /**
   * @var ComponentPluginManager
   */
  private ComponentPluginManager $componentManager;

  protected RendererInterface $renderer;

  /**
   * Datatype mappings for Shoelace components.
   *
   * @var array|string[]
   */
  private array $componentMapping = [
    'string' => 'textfield',
    'object' => 'textfield',
    'number' => 'number',
    'boolean' => 'checkbox',
  ];

  /**
   * @inheritDoc
   */
  public function getFormId()
  {
    return 'sholace_styleguide_preview';
  }

  /**
   * @param RendererInterface $rendererInterface
   * @param ComponentPluginManager $componentPluginManager
   */
  public function __construct(protected RendererInterface $rendererInterface, protected ComponentPluginManager $componentPluginManager) {
    $this->componentManager = $componentPluginManager;
    $this->renderer = $rendererInterface;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ShoelaceStyleguideForm|static {
    return new static(
      $container->get('renderer'),
      $container->get('plugin.manager.sdc')
    );
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $component = '') {
    $webComponent = $this->loadComponent($component);

    // Setup form render array.
    $form = [
      '#attached' => [
        'library' => [
          'shoelace_styleguide/shoelace_styleguide_preview',
        ],
      ],
      '#attributes' => [
        'class' => [
          'container',
        ],
      ],
      'component' => [
        'description' => [
          'content' => [
            '#markup' => '<h3>' . t($webComponent['name']) . '</h3>' .
              '<p>' . t($webComponent['description']) . '</p>' .
              '<hr><br>',
          ]
        ],
        'preview' => [
          '#tree' => TRUE,
          'component' => [
            '#type' => 'value',
            '#value' => $webComponent['id'],
          ],
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'row',
            ],
          ],
          'form' => [
            '#type' => 'container',
            '#attributes' => [
              'class' => [
                'col-xs-7',
              ],
            ],
            'component_id' => [
              '#type' => 'hidden',
              '#value' => $component,
            ],
          ]
        ]
      ]
    ];

    // Set slots fields.
    if (isset($webComponent['slots'])) {
      foreach ($webComponent['slots'] as $key => $field) {
        $form['component']['preview']['form'][$key] = [
          '#type' => 'textarea',
          '#title' => $key,
        ];
      }
    }

    // Set properties fields.
    if (isset($webComponent['props']['properties'])) {
      foreach ($webComponent['props']['properties'] as $key => $field) {

        if (is_array($field['type'])) {
          $type = $this->createOptions($field['type']);

          $form['component']['preview']['form'][$key . '_options']['data_type'] = [
            '#type' => 'radios',
            '#title' => $field['title'],
            '#description' => $field['description'],
            '#options' => $type
          ];
          $multipleTypes = TRUE;
        }

        if (isset($field['examples'])) {
          $examples = $this->createOptions($field['examples']);

          $form['component']['preview']['form'][$key] = [
            '#type' => 'select',
            '#title' => $field['title'],
            '#description' => $field['description'],
            '#options' => $examples,
            '#empty_option' => t('- Select -')
          ];
        }
        else {
          if (isset($multipleTypes) && $multipleTypes && is_array($field['type'])) {
            foreach ($field['type'] as $keyType => $value) {
              $form['component']['preview']['form'][$key . '_options'][$keyType][$key] = [
                '#type' => $this->componentMapping[$value],
                '#description' => $field['description'],
                '#title' => $field['title'],
                '#states' => [
                  'visible' => [
                    ':input[name="preview[form][' . $key . '_options][data_type]"]' => ['value' => $value],
                  ],
                ],
              ];
            }
          } else {
            $form['component']['preview']['form'][$key] = [
              '#type' => $this->componentMapping[$field['type']],
              '#description' => $field['description'],
              '#title' => $field['title'],
            ];
          }
        }
      }
    }

    // Set required fields.
    if (isset($singleComponent['props']['required'])) {
      foreach ($singleComponent['props']['required'] as $key => $field) {
        $form['component']['preview']['form'][$field]['#required'] = TRUE;
      }
    }

    $form['component']['preview']['form']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::onComponentSubmit',
        'event' => 'click',
        'wrapper' => 'result',
        'disable-refocus' => TRUE,
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Building component ...'),
        ],
      ],
      '#attributes' => [
        'type' => 'button',
      ],
    ];

    $form['component']['preview']['rendered_result'] = [
      '#attributes' => [
        'id' => 'result',
        'class' => 'col-xs-4',
      ],
      '#type' => 'container',
    ];

    return $form;
  }

  /**
   * AJAX handler for component values.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return mixed
   */
  public function onComponentSubmit(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // Scroll to the top command
    $response->addCommand(new ScrollTopCommand('body'));

    // Assuming $componentId is obtained from somewhere
    $values = $form_state->getUserInput()['preview']['form'];
    $jorge = $form_state->getUserInput();
    $rendered_result = [
      '#type' => 'component',
      '#component' => $values['component_id'],
      '#props' => $values,
    ];
    $renderedResultHtml = $this->renderer->renderRoot($rendered_result);

    $response->addCommand(new HtmlCommand('#result', $renderedResultHtml));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * @throws ComponentNotFoundException
   */
  protected function loadComponent(string $component): mixed {
    return $this->componentManager->find($component)->getPluginDefinition();
  }
}
