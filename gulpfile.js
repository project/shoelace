'use strict';
/*global require:true*/
/*jshint esversion: 6 */
const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass')(require('node-sass'));
const sassLint = require('gulp-sass-lint');
const jshint = require('gulp-jshint');
const stylish = require('jshint-stylish');
const gulpCopy = require('gulp-copy');
const replace = require('gulp-replace');
const $ = require('gulp-load-plugins')();
const refresh = require('gulp-refresh');

// Provide a path to node modules.
const sassPaths = [
  'node_modules'
];

// Copy materialize and tablesaw libraries.
function libsrc(done) {
  gulp.src([
    'node_modules/materialize-css/dist/js/materialize.min.js'
  ])
    .pipe(gulpCopy('js/vendor', { prefix: 4 }));
  gulp.src([
    'node_modules/tablesaw/dist/stackonly/tablesaw.stackonly.jquery.js'
  ])
    .pipe(gulpCopy('js/lib', { prefix: 4 }));
  gulp.src([
    'node_modules/tablesaw/dist/tablesaw-init.js'
  ])
    .pipe(gulpCopy('js/lib', { prefix: 3 }));
  done();
}
exports.libsrc = libsrc;

// Rename functions that conflict with jQueryUI; then move to source control folder js/lib
function rename() {
  return gulp.src(['js/vendor/materialize.min.js'])
    .pipe(replace('fn.autocomplete', 'fn.autocomplete_materialize'))
    .pipe(replace('r.autocomplete', 'r.autocomplete_materialize'))
    .pipe(replace('fn.tabs', 'fn.tabs_materialize'))
    .pipe(replace('.tabs(', '.tabs_materialize('))
    .pipe(gulp.dest('js/lib'));
}
exports.rename = rename;

// Lint and compile Sass.
function buildStyles(done) {
  return gulp.src(['scss/**/**.scss'])
    .pipe(sassLint({
      configFile: 'scss/.sass-lint.yml',
    }))
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.identityMap())
    .pipe(sass({
      includePaths: sassPaths
    })
      .on('error', sass.logError))
    .pipe($.autoprefixer())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('css'))
    .pipe(refresh());
  done();
}
exports.sass = buildStyles;

// Lint JavaScript files.
function lint() {
  return gulp.src(['./js/*.js', '!./js/vendor.all.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
}
exports.lint = lint;

function watch() {
  refresh.listen();
  return gulp.watch(['scss/**/*.scss'], gulp.series('sass'));
}
exports.watch = watch;

exports.default = gulp.series(buildStyles, watch);
